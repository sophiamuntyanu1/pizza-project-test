export const dataPizza = [
    {
        id: 1,
        imgUrl: '/images/pizzas/pizza1.svg',
        title: 'Итальянская',
        body: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 699},
            L: {size: '40', price: 979}
        },
        category: ['hot'],
        categoryUrl: ['/images/svg/hot.svg'],
        quantity: 0
    },
    {
        id: 2,
        imgUrl: '/images/pizzas/pizza2.svg',
        title: 'Маргарита',
        body: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        values: {
            S: {size: '20', price: 299},
            M: {size: '30', price: 400},
            L: {size: '40', price: 479}
        },
        category: ['meat'],
        categoryUrl: ['/images/svg/meat.svg'],
        quantity: 0
    },
    {
        id: 3,
        imgUrl: '/images/pizzas/pizza3.svg',
        title: 'Барбекю',
        body: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 699},
            L: {size: '40', price: 879}
        },
        category: ['meat'],
        categoryUrl: ['/images/svg/meat.svg'],
        quantity: 0
    },
    {
        id: 4,
        imgUrl: '/images/pizzas/pizza4.svg',
        title: 'Вегетарианская',
        body: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 479},
            L: {size: '40', price: 679}
        },
        category: ['veg'],
        categoryUrl: ['/images/svg/vegan.svg'],
        quantity: 0
    },
    {
        id: 5,
        imgUrl: '/images/pizzas/pizza5.svg',
        title: 'Мясная',
        body: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 699},
            L: {size: '40', price: 979}
        },
        category: ['meat', 'hot'],
        categoryUrl: ['/images/svg/meat.svg', '/images/svg/hot.svg'],
        quantity: 0
    },
    {
        id: 6,
        imgUrl: '/images/pizzas/pizza6.svg',
        title: 'Овощная',
        body: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 479},
            L: {size: '40', price: 679}
        },
        category: ['veg'],
        categoryUrl: ['/images/svg/vegan.svg'],
        quantity: 0
    },
    {
        id: 7,
        imgUrl: '/images/pizzas/pizza7.svg',
        title: 'Римская',
        body: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 699},
            L: {size: '40', price: 879}
        },
        category: ['meat'],
        categoryUrl: ['/images/svg/meat.svg'],
        quantity: 0
    },
    {
        id: 8,
        imgUrl: '/images/pizzas/pizza8.svg',
        title: 'С грибами',
        body: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 399},
            L: {size: '40', price: 679}
        },
        category: ['cheese'],
        categoryUrl: ['/images/svg/chease.svg'],
        quantity: 0
    },
    {
        id: 9,
        imgUrl: '/images/pizzas/pizza9.svg',
        title: 'Сырная',
        body: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 479},
            L: {size: '40', price: 699}
        },
        category: ['cheese'],
        categoryUrl: ['/images/svg/chease.svg'],
        quantity: 0
    },
    {
        id: 10,
        imgUrl: '/images/pizzas/pizza10.svg',
        title: 'Четыре сыра',
        body: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 479},
            L: {size: '40', price: 679}
        },
        category: ['cheese'],
        categoryUrl: ['/images/svg/chease.svg'],
        quantity: 0
    },
    {
        id: 11,
        imgUrl: '/images/pizzas/pizza11.svg',
        title: 'Пепперони Фреш с томатами',
        body: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        values: {
            S: {size: '20', price: 399},
            M: {size: '30', price: 699},
            L: {size: '40', price: 879}
        },
        category: ['hot'],
        categoryUrl: ['/images/svg/hot.svg'],
        quantity: 0
    },
    {
        id: 12,
        imgUrl: '/images/pizzas/pizza12.svg',
        title: 'Ветчина и сыр',
        body: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        values: {
            S: {size: '20', price: 299},
            M: {size: '30', price: 399},
            L: {size: '40', price: 679}
        },
        category: ['meat'],
        categoryUrl: ['/images/svg/meat.svg'],
        quantity: 0
    },
]
