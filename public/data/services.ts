export const servicesData = [
    {
        id: 1,
        title: 'Заказ',
        url: '/images/svg/1.svg',
        description: 'После оформления заказа мы&nbsp;свяжемся с&nbsp;вами для уточнения деталей.'
    },
    {
        id: 2,
        title: 'Доставка курьером',
        url: '/images/svg/2.svg',
        description: 'Мы&nbsp;доставим вашу пиццу горячей. Бесплатная доставка по&nbsp;городу.'
    },
    {
        id: 3,
        title: 'Оплата',
        url: '/images/svg/3.svg',
        description: 'Оплатить можно наличными или картой курьеру. И&nbsp;золотом тоже можно.'
    },
]