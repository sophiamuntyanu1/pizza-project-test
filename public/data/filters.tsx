import All from "@/(home)/_components/IconsFilters/All";
import Meat from "@/(home)/_components/IconsFilters/Meat";
import Hot from "@/(home)/_components/IconsFilters/Hot";
import Cheese from "@/(home)/_components/IconsFilters/Cheese";
import Vegan from "@/(home)/_components/IconsFilters/Vegan";
export const filtersData = [
    {
        id: 1,
        title: 'Все',
        url: '/images/svg/all.svg',
        icon: All,
        category: 'all'
    },
    {
        id: 2, title: 'Острые',
        url: '/images/svg/hot.svg',
        icon: Hot,
        category: 'hot'
    },
    {
        id: 3,
        title: 'Мясные',
        url: '/images/svg/meat.svg',
        icon: Meat,
        category: 'meat'
    },
    {
        id: 4,
        title: 'Сырные',
        url: '/images/svg/chease.svg',
        icon: Cheese,
        category: 'cheese'
    },
    {
        id: 5,
        title: 'Веганские',
        url: '/images/svg/vegan.svg',
        icon: Vegan,
        category: 'veg'
    },
]
