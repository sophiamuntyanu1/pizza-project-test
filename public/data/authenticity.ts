export const authenticityData = [
    {
        id: 1,
        title: 'Изготавливаем пиццу по&nbsp;своим рецептам в&nbsp;лучших традициях',
        url: '/images/reasons1.png',
        description: 'Наша пицца получается сочной, вкусной и&nbsp;главное хрустящей с&nbsp;нежной и&nbsp;аппетитной начинкой, готовим по&nbsp;своим итальянским рецептам'
    },
    {
        id: 2,
        title: 'Используем только свежие ингридиенты',
        url: '/images/reasons2.png',
        description: 'Ежедневно заготавливаем продукты и&nbsp;овощи для наших пицц, соблюдаем все сроки хранения'
    },
    {
        id: 3,
        title: 'Доставка в&nbsp;течение 60&nbsp;минут или заказ за&nbsp;наш счёт',
        url: '/images/reasons3.png',
        description: 'Все наши курьеры&nbsp;&mdash; фанаты серии Need for Speed и&nbsp;призеры гонок World Rally Championship и&nbsp;World Superbike во&nbsp;всех категориях'
    },
]