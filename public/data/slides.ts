export const slides = [
    {
        id:1,
        title:'Закажи 2 пиццы – 3-я в подарок',
        body:'При заказе 2-х больших пицц – средняя пицца в подарок',
        imgUrl:'/images/slide/item1.jpg'
    },
    {
        id:2,
        title:'Напиток в подарок',
        body:'Скидка на заказ от 3 000 рублей + напиток в подарок',
        imgUrl:'/images/slide/item2.jpg'
    },
    {
        id:3,
        title:'25% при первом заказе',
        body:'Скидка новым клиентам!',
        imgUrl:'/images/slide/item3.jpg'
    },
]
