import localFont from '@next/font/local';
export const alegreyaLocal = localFont({
    fallback: [
        'system-ui',
        'Segoe UI',
        'Roboto',
        'Helvetica',
        'Arial',
        'sans-serif',
        'Apple Color Emoji',
        'Segoe UI Emoji',
        'Segoe UI Symbol',
    ],
    display: 'swap',
    variable: '--alegreya-font',
    src: [
        {
            path: './Alegreya-Regular.woff2',
            weight: '1',
            style: 'normal',
        },
        {
            path: './Alegreya-Bold.woff2',
            weight: '700',
            style: 'normal',
        },
        {
            path: './Alegreya-ExtraBold.woff2',
            weight: '800',
            style: 'normal',
        },
        {
            path: './Alegreya-Black.woff2',
            weight: 'normal',
            style: 'normal',
        },
    ],
});

export const robotoLocal = localFont({
    fallback: [
        'system-ui',
        'Segoe UI',
        'Roboto',
        'Helvetica',
        'Arial',
        'sans-serif',
        'Apple Color Emoji',
        'Segoe UI Emoji',
        'Segoe UI Symbol',
    ],
    display: 'swap',
    variable: '--roboto-font',
    src: [
        {
            path: './Roboto.woff2',
            weight: 'normal',
            style: 'normal',
        },
    ],
});
