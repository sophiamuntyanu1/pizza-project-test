import '~/styles/index.sass';
import 'modern-normalize/modern-normalize.css';

import clsx from 'clsx';
import React, { ReactNode } from 'react';
import ReactQueryProvider from 'shared/providers/react-query';

import { CartProvider } from '@/(home)/_components/types/CartContext';
import { alegreyaLocal, robotoLocal } from '~/public/fonts/fonts';

export const metadata = {
    title: 'Pizza',
    description: 'Pizza',
};

export default function RootLayout({ children }: { children: ReactNode }) {
    return (
        <html
            lang="ru"
            className={clsx(alegreyaLocal.variable, robotoLocal.variable)}
            style={{ fontFamily: alegreyaLocal.style.fontFamily }}
        >
            <body>
                <main>
                    <ReactQueryProvider>
                        <CartProvider>{children}</CartProvider>
                    </ReactQueryProvider>
                </main>
            </body>
        </html>
    );
}
