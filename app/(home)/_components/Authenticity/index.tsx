import React from 'react';

import AuthenticityList from '@/(home)/_components/AuthenticityList';

import cn from './style.module.sass';

function Authenticity() {
    return (
        <div className={cn.authenticity}>
            <AuthenticityList />
        </div>
    );
}

export default Authenticity;
