'use client';

import Image from 'next/image';
import React from 'react';

import { useCart } from '@/(home)/_components/hooks/useCart';

import cn from './style.module.sass';

type CounterProps = {
    id: number;
    quantity: number;
};

function Counter({ id, quantity }: CounterProps) {
    const { decreaseQuantity, increaseQuantity } = useCart();
    return (
        <div className={cn.buttonsCounter}>
            <button className={cn.buttonCount} onClick={() => decreaseQuantity(id)}>
                <Image src={'/images/svg/minus.svg'} alt={'minus'} width={12} height={2} />
            </button>
            <input type="text" className={cn.inputCounter} value={quantity} />
            <button className={cn.buttonCount} onClick={() => increaseQuantity(id)}>
                <Image src={'/images/svg/plus.svg'} alt={'plus'} width={12} height={12} />
            </button>
        </div>
    );
}

export default Counter;
