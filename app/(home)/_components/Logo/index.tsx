import clsx from 'clsx';
import Image from 'next/image';
import React from 'react';

import cn from './style.module.sass';

interface Props {
    menuOpen: boolean;
    scroll: number;
}

const Logo = ({ menuOpen, scroll }: Props) => {
    return (
        <div
            className={clsx(
                cn.headerLogo,
                menuOpen && cn.headerLogoWhite,
                scroll && cn.headerLogoScroll
            )}
        >
            <a className={cn.logo} href="#">
                <Image src={'/images/svg/header-logo.svg'} alt={'img'} width={182} height={84} />
            </a>
        </div>
    );
};

export default Logo;
