import Image from 'next/image';
import React, { useEffect, useState } from 'react';

import { socialData } from '~/public/data/social';

import cn from './style.module.sass';

interface ItemsToShow {
    desktop: number;
    tablet: number;
    mobile: number;
}

const itemsToShow: ItemsToShow = {
    desktop: 10,
    tablet: 6,
    mobile: 4,
};

function Social() {
    const [currentItemsToShow, setCurrentItemsToShow] = useState<number | string>(
        itemsToShow.mobile
    );

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth >= 991) {
                setCurrentItemsToShow(itemsToShow.desktop);
            } else if (window.innerWidth >= 767) {
                setCurrentItemsToShow(itemsToShow.tablet);
            }
        };

        handleResize();
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);
    return (
        <div className={cn.social}>
            <div className={cn.socialText}>
                <h2 id="social" className={cn.socialMainTitle}>
                    Следите за&nbsp;нами в&nbsp;Instagram
                </h2>
                <a className={cn.socialLink} href="#" target="_blank">
                    @pizzamenu
                </a>
            </div>
            <div className={cn.socialList}>
                {socialData.slice(0, currentItemsToShow as number).map(item => {
                    return (
                        <div key={item.id} className={cn.socialItem}>
                            <Image src={item.url} alt={item.alt} width={384} height={384} />
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default Social;
