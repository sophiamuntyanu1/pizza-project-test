'use client';
import 'swiper/css';
import 'swiper/css/pagination';

import { Pagination } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';

import Slide from '@/(home)/_components/Slider/Slide';
import { slides } from '~/public/data/slides';

import cn from './style.module.sass';
function Slider() {
    return (
        <div className={cn.discounts}>
            <div className={cn.container}>
                <Swiper
                    pagination={{ clickable: true }}
                    className={cn.swiper}
                    modules={[Pagination]}
                    breakpoints={{
                        320: {
                            slidesPerView: 1,
                        },
                        576: {
                            slidesPerView: 2,
                            spaceBetween: 20,
                        },
                        767: {
                            slidesPerView: 3,
                            spaceBetween: 32,
                        },
                    }}
                >
                    {slides.map(slide => {
                        return (
                            <SwiperSlide key={slide.id}>
                                <Slide
                                    imgUrl={slide.imgUrl}
                                    body={slide.body}
                                    title={slide.title}
                                />
                            </SwiperSlide>
                        );
                    })}
                </Swiper>
            </div>
        </div>
    );
}

export default Slider;
