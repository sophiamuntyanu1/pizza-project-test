import Image from 'next/image';

import cn from './style.module.sass';

type SlideTypeProps = {
    imgUrl: string;
    body: string;
    title: string;
};

function Slide({ imgUrl, body, title }: SlideTypeProps) {
    return (
        <article className={cn.slideContainer}>
            <Image
                src={imgUrl}
                width={416}
                height={247}
                alt={'pizza'}
                className={cn.slideImage}
            ></Image>
            <div className={cn.slideTitle}>{title}</div>
            <div className={cn.slideDescription}>{body}</div>
        </article>
    );
}

export default Slide;
