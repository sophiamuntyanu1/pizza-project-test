import clsx from 'clsx';
import React from 'react';

import cn from './style.module.sass';

interface Props {
    scroll: number;
}
const Lang = ({ scroll }: Props) => {
    return (
        <a className={clsx(cn.headerLang, scroll && cn.headerLangNone)}>
            <span>en</span>
        </a>
    );
};

export default Lang;
