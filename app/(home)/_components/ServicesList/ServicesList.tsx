import Image from 'next/image';
import React from 'react';

import { servicesData } from '~/public/data/services';

import cn from './style.module.sass';

function ServicesList() {
    return (
        <div className={cn.spippingPayment}>
            <div className={cn.container}>
                <div className={cn.spippingPaymentList}>
                    {servicesData.map(item => {
                        return (
                            <div key={item.id} className={cn.spippingPaymentItem}>
                                <div className={cn.spippingPaymentImg}>
                                    <Image src={item.url} alt={'img'} fill={true} />
                                </div>
                                <div className={cn.spippingPaymentContent}>
                                    <div className={cn.spippingPaymentTitle}>{item.title}</div>
                                    <div
                                        dangerouslySetInnerHTML={{ __html: item.description || '' }}
                                        className={cn.spippingPaymentDescription}
                                    ></div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
}

export default ServicesList;
