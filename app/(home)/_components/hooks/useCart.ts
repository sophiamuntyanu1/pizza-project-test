import { useContext } from 'react';

import { CartContext } from '@/(home)/_components/context/CartContext';

export function useCart() {
    return useContext(CartContext);
}
