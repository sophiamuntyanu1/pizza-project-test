import React from 'react';
import { Link } from 'react-scroll';

import cn from './style.module.sass';

type CustomImageProps = {
    src: string;
    alt: string;
    mockWidth: number;
    mockHeight: number;
};

const Image = ({ src, alt, mockWidth, mockHeight }: CustomImageProps) => {
    return <img src={src} alt={alt} width={mockWidth} height={mockHeight} />;
};
function Banner({ headerHeight }: { headerHeight: number }) {
    return (
        <div className={cn.product} style={{ marginTop: headerHeight }}>
            <div className={cn.productBoxImg}>
                <div className={cn.productImg}>
                    <Image
                        src={'/images/banner.png'}
                        alt={'img'}
                        mockWidth={1007}
                        mockHeight={630}
                    />
                </div>
            </div>
            <div className={cn.containerProduct}>
                <div className={cn.productContent}>
                    <h1 className={cn.productTitle}>Пицца на&nbsp;заказ</h1>
                    <p className={cn.productDescription}>
                        Бесплатная и&nbsp;быстрая доставка за&nbsp;час в&nbsp;любое удобное для вас
                        время
                    </p>
                    <Link
                        className={cn.productBtn}
                        activeClass="active"
                        to="pizza"
                        spy={true}
                        smooth={true}
                        offset={-70}
                        duration={500}
                    >
                        Выбрать пиццу
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Banner;
