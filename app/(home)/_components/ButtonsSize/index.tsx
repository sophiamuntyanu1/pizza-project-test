import clsx from 'clsx';
import React from 'react';

import { Size } from '@/(home)/_components/types/types';

import cn from './style.module.sass';

type SizeButtonsProps = {
    setSize: (arg: Size) => void;
    size: Size;
};

function ButtonsSize({ setSize, size }: SizeButtonsProps) {
    return (
        <div className={cn.pizzaSize}>
            <div className={cn.pizzaTitleSize}>Размер, см:</div>
            <div className={cn.pizzaBtnsSize}>
                <button
                    className={clsx(cn.pizzaBtn, {
                        [cn.buttonActive as string]: size === 'S',
                    })}
                    onClick={() => setSize('S')}
                >
                    20
                </button>
                <button
                    className={clsx(cn.pizzaBtn, {
                        [cn.buttonActive as string]: size === 'M',
                    })}
                    onClick={() => setSize('M')}
                >
                    30
                </button>
                <button
                    className={clsx(cn.pizzaBtn, {
                        [cn.buttonActive as string]: size === 'L',
                    })}
                    onClick={() => setSize('L')}
                >
                    40
                </button>
            </div>
        </div>
    );
}

export default ButtonsSize;
