import React, { useState } from 'react';

import ButtonsSize from '@/(home)/_components/ButtonsSize';
import { useCart } from '@/(home)/_components/hooks/useCart';
import PizzaImg from '@/(home)/_components/PizzaImg';
import { Size, ValuesType } from '@/(home)/_components/types/types';

import cn from './style.module.sass';
type PizzaCardProps = {
    id: number;
    imgUrl: string;
    title: string;
    body: string | string[];
    values: ValuesType;
    category: string[];
    categoryUrl: string[];
};

function ProductCard({ title, body, imgUrl, categoryUrl, values, id }: PizzaCardProps) {
    const { addToCart } = useCart();
    const [size, setSize] = useState<Size>('M');
    const onclickHandler = () => {
        addToCart(id, values[size], title);
    };

    const [isHover, setIsHover] = useState(false);

    return (
        <div
            className={cn.cardItem}
            onMouseEnter={() => {
                setIsHover(true);
            }}
            onMouseLeave={() => {
                setIsHover(false);
            }}
        >
            <div className={cn.titleContainer}>
                <PizzaImg
                    isHover={isHover}
                    size={size}
                    categoryUrl={categoryUrl}
                    imgUrl={imgUrl}
                ></PizzaImg>
            </div>
            <div className={cn.textContainer}>
                <div className={cn.textContainer}>
                    <div className={cn.pizzaName}>{title}</div>
                    <div className={cn.pizzaDescription}>{body}</div>
                </div>
                <ButtonsSize setSize={setSize} size={size} />
                <div className={cn.price}>от {values[size]?.price} руб.</div>
                <button className={cn.orderButton} onClick={onclickHandler}>
                    <div className={cn.orderTitle}>Заказать</div>
                    <div className={cn.orderPriceMobile}>от {values[size]?.price} руб.</div>
                </button>
            </div>
        </div>
    );
}
export default ProductCard;
