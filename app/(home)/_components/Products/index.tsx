'use client';
import React, { useState } from 'react';

import FiltersPizzas from '@/(home)/_components/FiltersPizzas';
import ProductCard from '@/(home)/_components/Products/ProductCard';
import { dataPizza } from '~/public/data/dataPizza';

import cn from './style.module.sass';
function Products() {
    const initialData = dataPizza;
    const [visibleData, setVisibleData] = useState(initialData);
    const filterCategories = (category: string) => {
        if (!category || category === 'all') {
            setVisibleData(initialData);
            return;
        }
        const NewVisibleData = initialData.filter(item => item.category.includes(category));
        setVisibleData(NewVisibleData);
    };

    return (
        <div>
            <h2 id="pizza" className={cn.pizzaTitle}>
                Выберите пиццу
            </h2>
            <FiltersPizzas filterCategories={filterCategories} />
            <div className={cn.container}>
                <div className={cn.pizzaList}>
                    {visibleData.length > 0 ? (
                        visibleData.map(item => {
                            const { id, title, body, imgUrl, categoryUrl, category, values } = item;
                            return (
                                <ProductCard
                                    key={id}
                                    id={id}
                                    imgUrl={imgUrl}
                                    title={title}
                                    body={body}
                                    values={values}
                                    category={category}
                                    categoryUrl={categoryUrl}
                                ></ProductCard>
                            );
                        })
                    ) : (
                        <p className={cn.pizzaListNone}>No products available</p>
                    )}
                </div>
            </div>
        </div>
    );
}

export default Products;
