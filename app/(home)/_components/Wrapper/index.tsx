'use client';
import React, { useState } from 'react';

import Authenticity from '@/(home)/_components/Authenticity';
import Banner from '@/(home)/_components/Banner';
import Footer from '@/(home)/_components/Footer';
import Header from '@/(home)/_components/Header';
import Products from '@/(home)/_components/Products';
import Services from '@/(home)/_components/Services';
import Slider from '@/(home)/_components/Slider';
import Social from '@/(home)/_components/Social';

import cn from './style.module.sass';

function Wrapper() {
    const [headerHeight, setHeaderHeight] = useState(0);
    return (
        <div className={cn.wrapper}>
            <Header setHeaderHeight={setHeaderHeight} />
            <Banner headerHeight={headerHeight} />
            <Slider />
            <Products />
            <Services />
            <Authenticity />
            <Social />
            <Footer />
        </div>
    );
}
export default Wrapper;
