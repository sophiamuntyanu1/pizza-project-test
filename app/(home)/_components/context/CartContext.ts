'use client';
import { createContext } from 'react';

import { CartContextType } from '@/(home)/_components/types/types';

// eslint-disable-next-line @typescript-eslint/no-unused-vars,unused-imports/no-unused-vars
export const CartContext = createContext({} as CartContextType);
