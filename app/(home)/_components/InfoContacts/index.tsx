import React from 'react';

type InfoBlockProps = {
    className: string | undefined;
    children: React.ReactNode;
};
const InfoContacts: React.FC<InfoBlockProps> = ({ className, children }) => {
    return <div className={className}>{children}</div>;
};

export default InfoContacts;
