import clsx from 'clsx';
import Image from 'next/image';
import React from 'react';

import cn from './style.module.sass';

type ImageProps = {
    categoryUrl: string[];
    imgUrl: string;
    size?: string;
};

const CartImg = ({ categoryUrl, imgUrl, size }: ImageProps) => {
    return (
        <div className={cn.imgContainer}>
            <div className={cn.icon}>
                <Image src={categoryUrl[0] as string} width={12} height={12} alt={'icon'}></Image>
            </div>
            <Image
                src={'/images/Size.png'}
                className={cn.pizzaBorder}
                width={83.3}
                height={83.3}
                alt={'pizza border'}
            />
            <Image
                src={imgUrl}
                width={77.22}
                height={77.22}
                alt={'pizza'}
                className={clsx(cn.pizza, {
                    [`${cn.sizeS}`]: size == '20',
                    [cn.sizeM as string]: size == '30',
                    [cn.sizeL as string]: size == '40',
                })}
            />
        </div>
    );
};

export default CartImg;
