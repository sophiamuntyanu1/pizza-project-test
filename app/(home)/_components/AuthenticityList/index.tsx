import Image from 'next/image';
import React from 'react';

import { authenticityData } from '~/public/data/authenticity';

import cn from './style.module.sass';
function ServicesList() {
    return (
        <div className={cn.authenticityList}>
            {authenticityData.map(item => {
                return (
                    <div key={item.id} className={cn.authenticityItem}>
                        <div className={cn.authenticityImg}>
                            <Image src={item.url} alt={'img'} fill={true} />
                        </div>
                        <div className={cn.authenticityContent}>
                            <div
                                dangerouslySetInnerHTML={{ __html: item.title || '' }}
                                className={cn.authenticityTitle}
                            ></div>
                            <div
                                dangerouslySetInnerHTML={{ __html: item.description || '' }}
                                className={cn.authenticityDescription}
                            ></div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

export default ServicesList;
