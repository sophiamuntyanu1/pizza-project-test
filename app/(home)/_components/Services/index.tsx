import React from 'react';

import ServicesList from '@/(home)/_components/ServicesList/ServicesList';

import cn from './style.module.sass';
function Services() {
    return (
        <div className={cn.spippingPayment}>
            <div className={cn.container}>
                <h2 id="spipping-payment" className={cn.spippingPaymentMainTitle}>
                    Доставка и&nbsp;оплата
                </h2>
                <ServicesList />
            </div>
        </div>
    );
}

export default Services;
