import React from 'react';

import cn from './style.module.sass';

function Information() {
    return (
        <div className={cn.information}>
            <div className={cn.clientInfo}>
                <a className={cn.clientInfoBox} href="tel:+79184326587">
                    <div className={cn.clientInfoImg}></div>
                    <div className={cn.clientInfoContent}>
                        <div className={cn.clientInfoTitle}>+7 (918) 432-65-87</div>
                        <div className={cn.clientInfoDescription}>
                            Ежедневно с&nbsp;9:00 до&nbsp;23:00
                        </div>
                    </div>
                </a>
            </div>

            <div className={cn.clientInfo}>
                <a className={cn.clientInfoBox} href="#" target="_blank">
                    <div className={cn.clientInfoImg}>
                        <div className={cn.orderCount}>3</div>
                    </div>
                    <div className={cn.clientInfoContent}>
                        <div className={cn.clientInfoTitle}>Ваш заказ</div>
                        <div className={cn.clientInfoDescription}>
                            Итальянская и&nbsp;ещё 2&nbsp;пиццы
                        </div>
                    </div>
                </a>
            </div>
        </div>
    );
}

export default Information;
