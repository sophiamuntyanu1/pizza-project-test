'use client';
import clsx from 'clsx';
import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-scroll';

import Cart from '@/(home)/_components/Cart';
import { useCart } from '@/(home)/_components/hooks/useCart';
import InfoContacts from '@/(home)/_components/InfoContacts';
import Lang from '@/(home)/_components/Lang/Lang';
import Logo from '@/(home)/_components/Logo';
import Modal from '@/(home)/_components/Order';
import { contacts } from '~/public/data/contacts';

import cn from './style.module.sass';

type headerHeight = {
    setHeaderHeight: React.Dispatch<React.SetStateAction<number>>;
};
function Header({ setHeaderHeight }: headerHeight) {
    const { cartQuantity, cartItems } = useCart();
    const menuItems = [
        { text: 'Меню', target: 'pizza' },
        { text: 'О нас', target: 'spipping-payment' },
        { text: 'Контакты', target: 'social' },
    ];

    const headerRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (headerRef.current) {
            setHeaderHeight(headerRef.current.clientHeight);
        }

        const handleResize = () => {
            if (headerRef.current) {
                setHeaderHeight(headerRef.current.clientHeight);
            }
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, [setHeaderHeight]);

    const [isModalOpen, setIsModalOpen] = useState(false);
    const closeModal = () => {
        setIsModalOpen(false);
    };
    const [menuOpen, setMenuOpen] = useState(false);
    const toggleMenu = () => {
        setMenuOpen(!menuOpen);
    };

    const useScroll = () => {
        const [position, setPosition] = useState(0);

        useEffect(() => {
            const handleScroll = () => {
                setPosition(window.scrollY);
            };

            window.addEventListener('scroll', handleScroll);
            handleScroll();

            return () => {
                window.removeEventListener('scroll', handleScroll);
            };
        }, []);

        return position;
    };

    const scroll = useScroll();

    return (
        <div
            ref={headerRef}
            className={clsx(cn.header, {
                [`${cn.headerScroll}`]: scroll,
            })}
        >
            <div className={cn.headerContainer}>
                <Logo menuOpen={menuOpen} scroll={scroll} />
                <div className={cn.headerContent}>
                    <div className={cn.menu}>
                        <div
                            role="presentation"
                            className={clsx(cn.icon, menuOpen && cn.open)}
                            onClick={toggleMenu}
                        >
                            <span></span>
                        </div>
                        <nav className={clsx(cn.body, menuOpen && cn.open)}>
                            <ul className={cn.list}>
                                {menuItems.map((item, index) => (
                                    <li key={`${index}-${item.text}`} className={cn.item}>
                                        <Link
                                            className={cn.link}
                                            activeClass="active"
                                            to={item.target}
                                            spy={true}
                                            smooth={true}
                                            offset={-70}
                                            duration={500}
                                            onClick={toggleMenu}
                                        >
                                            {item.text}
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                            <div className={cn.informationMob}>
                                <p>Заказать по&nbsp;телефону</p>
                                <InfoContacts className={cn.infoContacts}>
                                    {contacts.map(item => {
                                        return (
                                            <div key={item.id}>
                                                <a href={item.href} className={cn.contactsTitle}>
                                                    {item.tel}
                                                </a>
                                                <div className={cn.contactsDescription}>
                                                    {item.title}
                                                </div>
                                            </div>
                                        );
                                    })}
                                </InfoContacts>
                                <div className={cn.menuLang}>
                                    <a href="#">English</a>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div className={cn.information}>
                        <div className={cn.clientInfo}>
                            <a className={cn.clientInfoBox} href="tel:+79184326587">
                                <div className={cn.clientInfoImg}>
                                    <img src="/images/svg/pphone.svg" alt="phone" />
                                </div>
                                <InfoContacts className={cn.clientInfoContent}>
                                    {contacts.map(item => {
                                        return (
                                            <div key={item.id}>
                                                <a href={item.href} className={cn.clientInfoTitle}>
                                                    {item.tel}
                                                </a>
                                                <div className={cn.clientInfoDescription}>
                                                    {item.title}
                                                </div>
                                            </div>
                                        );
                                    })}
                                </InfoContacts>
                            </a>
                        </div>
                        <div className={cn.clientInfo}>
                            <div
                                role="presentation"
                                className={cn.clientInfoBox}
                                onClick={() => setIsModalOpen(true)}
                            >
                                <div className={cn.clientInfoImg}>
                                    <img src="/images/svg/Cart.svg" alt="cart" />
                                    <div className={cn.orderCount}>{cartQuantity}</div>
                                </div>
                                <div className={cn.clientInfoContent}>
                                    <div className={cn.clientInfoTitle}>Ваш заказ</div>
                                    <div className={cn.clientInfoDescription}>
                                        {cartItems.length > 0 ? (
                                            <div>
                                                {cartItems.length > 1
                                                    ? `${cartItems[0]?.title} и еще ${
                                                          cartItems.length - 1
                                                      } пиццы`
                                                    : cartItems[0]?.title}
                                            </div>
                                        ) : (
                                            <div>Корзина пуста</div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Lang scroll={scroll} />
                        <Modal isModalOpen={isModalOpen} closeModal={closeModal}>
                            <Cart closeModal={closeModal} />
                        </Modal>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header;
