import clsx from 'clsx';
import React from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';

import cn from './style.module.sass';

type Inputs = {
    name: string;
    phone: number;
    email: string;
};
function Form() {
    const errorMessage = 'Обязательное поле';
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<Inputs>({ mode: 'onBlur' });
    const onSubmit: SubmitHandler<Inputs> = () => {
        alert(`Ваш заказ оформлен!`);
    };
    return (
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        <form className={cn.formContainer} onSubmit={handleSubmit(onSubmit)}>
            <div className={cn.formTitle}>Контакты</div>
            <div className={cn.formInputs}>
                <div className={cn.formInputsGroup}>
                    <div className={cn.formInput}>
                        <input
                            id="name"
                            type="text"
                            placeholder={'Имя'}
                            className={clsx(cn.input, errors.name?.message && cn.inputError)}
                            {...register('name', {
                                required: errorMessage,
                                maxLength: 10,
                                pattern: {
                                    value: /^[а-яА-Я]/,
                                    message: 'Имя должно состоять из букв',
                                },
                            })}
                        />
                        <label htmlFor="name" className={cn.placeholder}>
                            Имя
                        </label>
                        {errors?.name && (
                            <div className={cn.errorMessage}>
                                {errors?.name?.message ||
                                    'Кол-во символов не должно быть больше 10'}
                            </div>
                        )}
                    </div>
                    <div className={cn.formInput}>
                        <input
                            id="phone"
                            {...register('phone', {
                                required: errorMessage,
                                pattern: {
                                    value: /[0-9\\.,:]/,
                                    message: 'Только цифры',
                                },
                            })}
                            placeholder={'Телефон'}
                            className={
                                errors.name?.message ? `${cn.input} ${cn.inputError}` : cn.input
                            }
                        />
                        <label htmlFor="phone" className={cn.placeholder}>
                            Телефон
                        </label>
                        {errors?.phone && (
                            <div className={cn.errorMessage}>
                                {errors?.phone?.message || 'Ошибка!'}
                            </div>
                        )}
                    </div>
                </div>
                <div className={cn.formInput}>
                    <input
                        id="email"
                        type="text"
                        placeholder={'Адрес доставки'}
                        {...register('email', {
                            required: errorMessage,
                            pattern: {
                                value: /^[a-zA-Z]/,
                                message: 'Должны быть латинские буквы',
                            },
                        })}
                        className={errors.name?.message ? clsx(cn.input, cn.inputError) : cn.input}
                    />
                    <label htmlFor="email" className={cn.placeholder}>
                        Адрес доставки
                    </label>
                    {errors?.email && (
                        <div className={cn.errorMessage}>{errors?.email?.message || 'Ошибка!'}</div>
                    )}
                </div>
            </div>
            <div className={cn.formPayment}>
                <div className={cn.formPaymentTitle}>Способы оплаты</div>
                <div className={cn.formRadioInputs}>
                    <div className={cn.formRadioInput}>
                        <input
                            type="radio"
                            name={'payment'}
                            id="paymentChoice1"
                            className={cn.formRadioButton}
                        />
                        <div className={cn.formCustomInput} />
                        <label htmlFor="paymentChoice1" className={cn.formRadioInputLabel}>
                            Оплата наличными или картой курьеру
                        </label>
                    </div>
                    <div className={cn.formRadioInput}>
                        <input
                            className={cn.formRadioButton}
                            type="radio"
                            name={'payment'}
                            id="paymentChoice2"
                        />
                        <div className={cn.formCustomInput} />
                        <label htmlFor="paymentChoice2" className={cn.formRadioInputLabel}>
                            Оплата картой онлайн на сайте
                        </label>
                    </div>
                </div>
            </div>
            <button type="submit" className={cn.formBtn}>
                Оформить заказ
            </button>
            <div className={cn.formDisclaimer}>
                Нажимая кнопку &laquo;Оформить заказ&raquo; вы&nbsp;соглашаетесь с&nbsp;политикой
                конфиденциальности
            </div>
        </form>
    );
}

export default Form;
