import Image from 'next/image';
import React from 'react';

import CartImg from '@/(home)/_components/CartImg';
import Counter from '@/(home)/_components/Counter';
import { useCart } from '@/(home)/_components/hooks/useCart';
import { dataPizza } from '~/public/data/dataPizza';

import cn from './style.module.sass';
type CartItemProps = {
    id: number;
    quantity: number;
    price: number;
    size?: string;
};

function CartItem({ id, quantity, price, size }: CartItemProps) {
    const { removeFromCart } = useCart();
    const item = dataPizza.find(item => item.id === id);
    const onclickHandler = () => {
        removeFromCart(id);
    };

    const totalSum = quantity * price;
    // eslint-disable-next-line lodash/prefer-is-nil
    if (item === null || item === undefined) {
        return null;
    }
    return (
        <div className={cn.cartWrapper}>
            <div className={cn.cartContainer}>
                <div className={cn.cartContent}>
                    <div className={cn.cartImg}>
                        <CartImg
                            categoryUrl={item.categoryUrl}
                            imgUrl={item.imgUrl}
                            size={size}
                        ></CartImg>
                        <div className={cn.cartText}>
                            <div className={cn.cartTitle}>{item.title}</div>
                            <div className={cn.cartSize}>{size} см.</div>
                        </div>
                    </div>
                    <div className={cn.cartInformation}>
                        <div className={cn.cartCounter}>
                            <Counter id={id} quantity={quantity}></Counter>
                        </div>
                        <div className={cn.cartTotalSumContainer}>
                            <div className={cn.cartTotalSum}>{totalSum}</div>
                            <div className={cn.cartCurrent}>руб</div>
                        </div>
                    </div>
                </div>
                <button className={cn.deleteBtn} onClick={onclickHandler}>
                    <Image
                        src={'/images/svg/delete.svg'}
                        width={24}
                        height={24}
                        alt={'delete icon'}
                    ></Image>
                </button>
            </div>
        </div>
    );
}

export default CartItem;
