import Image from 'next/image';
import React from 'react';

import CartItem from '@/(home)/_components/Cart/CartItem';
import Form from '@/(home)/_components/Form';
import { useCart } from '@/(home)/_components/hooks/useCart';

import cn from './style.module.sass';
type CartProps = {
    closeModal: () => void;
};

function Cart({ closeModal }: CartProps) {
    const { cartItems } = useCart();
    return (
        <div className={cn.cartContainer}>
            {cartItems.length > 0 ? (
                <div>
                    <div className={cn.cartHeader}>
                        <div className={cn.cartTitle}>Ваш заказ</div>
                        <button className={cn.closeBtn} onClick={() => closeModal()}>
                            <Image
                                src={'/images/svg/close.svg'}
                                width={40}
                                height={40}
                                alt={'close-button'}
                            ></Image>
                        </button>
                    </div>
                    <div className={cn.cartBody}>
                        {cartItems.map(item => {
                            const { id, quantity, values } = item;
                            return (
                                <CartItem
                                    key={id}
                                    id={id}
                                    quantity={quantity}
                                    price={values.price}
                                    size={values?.size}
                                />
                            );
                        })}
                    </div>
                    <div className={cn.totalPrice}>
                        <div className={cn.sumHeading}>Сумма заказа:</div>
                        <div className={cn.sumTotal}>
                            {cartItems.reduce((total, cartItem) => {
                                const item = cartItems.find(item => item.id === cartItem.id);
                                return total + item!.values.price * cartItem.quantity;
                            }, 0)}
                            <div className={cn.sumCurrency}>руб</div>
                        </div>
                    </div>
                    <Form />
                </div>
            ) : (
                <div>
                    <div className={cn.cartHeader}>
                        <div className={cn.cartTitle}>Ваша корзина пуста :(</div>
                        <button className={cn.closeBtn} onClick={() => closeModal()}>
                            <Image
                                src={'/images/svg/close.svg'}
                                width={40}
                                height={40}
                                alt={'close-button'}
                            ></Image>
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
}

export default Cart;
