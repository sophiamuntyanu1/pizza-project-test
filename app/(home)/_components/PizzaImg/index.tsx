import clsx from 'clsx';
import Image from 'next/image';
import React from 'react';

import { Size } from '@/(home)/_components/types/types';

import cn from './style.module.sass';

type ImageProps = {
    categoryUrl: string[];
    imgUrl: string;
    size: Size;
    isHover: boolean;
};

const PizzaImg = ({ categoryUrl, imgUrl, size, isHover }: ImageProps) => {
    return (
        <div className={cn.imageBox}>
            <div className={isHover ? clsx(cn.icons, cn.hover) : cn.icons}>
                {categoryUrl.map(icon => (
                    <Image
                        key={icon}
                        src={icon}
                        width={24}
                        height={24}
                        alt="icon"
                        className={cn.icon}
                    />
                ))}
            </div>
            <div className={cn.imageContainer}>
                <Image
                    src="/images/Size.png"
                    className={cn.pizzaBorder}
                    width={200}
                    height={200}
                    alt="pizza border"
                />
                <div>
                    <Image
                        src={imgUrl}
                        width={155}
                        height={155}
                        alt="pizza"
                        className={clsx(cn.sizePizza, {
                            [cn.sizePizzaS as string]: size === 'S',
                            [cn.sizePizzaM as string]: size === 'M',
                            [cn.sizePizzaL as string]: size === 'L',
                        })}
                    />
                </div>
            </div>
        </div>
    );
};

export default PizzaImg;
