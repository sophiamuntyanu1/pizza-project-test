import Image from 'next/image';
import React from 'react';

import cn from './style.module.sass';

function Footer() {
    return (
        <div className={cn.footer}>
            <div className={cn.container}>
                <div className={cn.footerContent}>
                    <div className={cn.footerInfo}>
                        <a className={cn.footerLogo} href="#">
                            <Image
                                src={'/images/svg/logo-white.svg'}
                                alt={'img'}
                                width={112}
                                height={52}
                            />
                        </a>
                        <div className={cn.footerContacts}>
                            <div className={cn.footerPhone}>
                                <a href="tel:+79184326587">+7 (918) 432-65-87</a>
                            </div>
                            <div className={cn.footerShedule}>
                                Ежедневно с&nbsp;9:00 до&nbsp;23:00
                            </div>
                        </div>
                    </div>
                    <div className={cn.footerPolitics}>
                        <a href="#">Политика конфиденциальности</a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;
