'use client';
import React from 'react';

import cn from './style.module.sass';

function Menu() {
    const menuItems = [
        { text: 'Меню', target: 'pizza' },
        { text: 'О нас', target: 'spipping-payment' },
        { text: 'Контакты', target: 'social' },
    ];
    return (
        <div className={cn.menu}>
            <div className={cn.icon}>
                <span></span>
            </div>
            <nav className={cn.body}>
                <ul className={cn.list}>
                    {menuItems.map((item, index) => (
                        <li key={index} className={cn.item}>
                            <a className={cn.link} href="#" data-goto={item.target}>
                                {item.text}
                            </a>
                        </li>
                    ))}
                </ul>
                <div className={cn.informationMob}>
                    <p>Заказать по&nbsp;телефону</p>
                    <div className={cn.infoContacts}>
                        <a href="tel:+79184326587" className={cn.contactsTitle}>
                            +7 (918) 432-65-87
                        </a>
                        <div className={cn.contactsDescription}>
                            Ежедневно с&nbsp;9:00 до&nbsp;23:00
                        </div>
                    </div>
                    <div className={cn.menuLang}>
                        <a href="#">English</a>
                    </div>
                </div>
            </nav>
        </div>
    );
}

export default Menu;
