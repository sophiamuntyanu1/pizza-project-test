export type Size = 'S' | 'M' | 'L';

export type ValuesItem = { size: string; price: number };

export type ValuesType = {
    [key in Size]: ValuesItem;
};

export type ItemType = {
    id: number;
    values: ValuesItem;
    quantity: number;
    title?: string;
    body?: string;
};
export type CartContextType = {
    cartItems: ItemType[];
    addToCart: (id: number, currentValues: ValuesItem, title: string) => void;
    removeFromCart: (id: number) => void;
    cartQuantity: number;
    getItemQuantity: (id: number) => void;
    decreaseQuantity: (id: number) => void;
    increaseQuantity: (id: number, currentValues?: ValuesItem) => void;
};
