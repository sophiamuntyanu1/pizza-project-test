'use client';
import { ReactNode, useState } from 'react';

import { CartContext } from '@/(home)/_components/context/CartContext';
import { ItemType, ValuesItem } from '@/(home)/_components/types/types';

type CartProviderProps = {
    children: ReactNode;
};

export function CartProvider({ children }: CartProviderProps) {
    const [cartItems, setCartItems] = useState<ItemType[]>([]);

    const cartQuantity = cartItems.reduce((quantity, item) => item.quantity + quantity, 0);
    const getItemQuantity = (id: number) => {
        return cartItems.find(item => item.id === id)?.quantity || 0;
    };

    const removeFromCart = (id: number) => {
        setCartItems(currentItems => {
            return currentItems.filter(item => item.id !== id);
        });
    };

    const addToCart = (id: number, currentValues: ValuesItem, title: string) => {
        setCartItems(currentItems => {
            if (currentItems.find(item => item.id === id) == null) {
                return [...currentItems, { id, quantity: 1, values: currentValues, title }];
            } else {
                return currentItems.map(item => {
                    if (item.id === id) {
                        return { ...item, quantity: item.quantity + 1 };
                    } else return item;
                });
            }
        });
    };
    const increaseQuantity = (id: number, currentValues: ValuesItem) => {
        setCartItems(currentItems => {
            if (currentItems.find(item => item.id === id) == null) {
                return [...currentItems, { id, quantity: 1, values: currentValues }];
            } else {
                return currentItems.map(item => {
                    if (item.id === id) {
                        return { ...item, quantity: item.quantity + 1 };
                    } else return item;
                });
            }
        });
    };

    const decreaseQuantity = (id: number) => {
        setCartItems(currentItems => {
            if (currentItems.find(item => item.id === id)?.quantity == 1) {
                return currentItems.filter(item => item.id !== id);
            } else {
                return currentItems.map(item => {
                    if (item.id === id) {
                        return { ...item, quantity: item.quantity - 1 };
                    } else return item;
                });
            }
        });
    };

    return (
        <CartContext.Provider
            value={{
                cartItems,
                addToCart,
                cartQuantity,
                removeFromCart,
                getItemQuantity,
                decreaseQuantity,
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                increaseQuantity,
            }}
        >
            {children}
        </CartContext.Provider>
    );
}
