import React from 'react';

import Information from '@/(home)/_components/Information';
import Menu from '@/(home)/_components/Menu';

import cn from './style.module.sass';

function Content() {
    return (
        <div className={cn.headerContent}>
            <Menu />
            <Information />
        </div>
    );
}

export default Content;
