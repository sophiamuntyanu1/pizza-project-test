import React, { ReactNode } from 'react';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import ReactModal from 'react-modal';
type ModalProps = {
    isModalOpen: boolean;
    closeModal: () => void;
    children: ReactNode;
};

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        maxWidth: '100%',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        zIndex: '100',
        overflow: 'hidden',
        height: 'auto',
    },
    overlay: {
        background: 'rgba(0,0,0, 0.9)',
        zIndex: '99',
    },
};

const Modal = ({ isModalOpen, closeModal, children }: ModalProps) => {
    return (
        <ReactModal isOpen={isModalOpen} style={customStyles} onRequestClose={closeModal}>
            {children}
        </ReactModal>
    );
};

export default Modal;
