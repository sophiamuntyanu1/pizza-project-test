import React from 'react';

import { filtersData } from '~/public/data/filters';

import cn from './style.module.sass';

type FilterProps = {
    filterCategories: (category: string) => void;
};

function FiltersPizzas({ filterCategories }: FilterProps) {
    return (
        <div className={cn.pizzaTastes}>
            {filtersData.length > 0 &&
                filtersData.map(({ id, category, title, icon: Icon }) => {
                    return (
                        <button
                            key={id}
                            className={cn.pizzaTaste}
                            onClick={() => filterCategories(category)}
                        >
                            <div className={cn.pizzaBlock}>
                                <Icon className={cn.pizzaIcon} />
                                <div className={cn.pizzaHeading}>{title}</div>
                            </div>
                        </button>
                    );
                })}
        </div>
    );
}

export default FiltersPizzas;
