import Flex from 'core/Flex';
import React, { ReactNode } from 'react';

function HomeLayout({ children }: { children: ReactNode }) {
    return <Flex>{children}</Flex>;
}

export default HomeLayout;
